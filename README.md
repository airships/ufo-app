# LittleUFO App

Nothing fancy here, lots to do...

## Features
* Start/finish fleets
* Display fleet composition
* Send intel channel logs to LittleUFO

## Build
On Windows, simply run `__build.cmd` script.  
This will create `ufo-app.exe` file in `build` directory.

## Run for testing
To run it, use `__run.cmd` script or `__build.cmd run`.  
This will create temporary `ufo-app-test.exe` file in `build` directory.

## Contacts
You probably know me if you're here :)
