package ufo

import (
    "bytes"
    "encoding/json"
    "fmt"
    "io"
    "io/ioutil"
    "net/http"
    "net/url"
    "os"
    "time"
)

// User object
var User map[string]interface{}

// UserEve sub-object
var UserEve map[string]interface{}

// UserQuery refreshes User and UserEve
func UserQuery() {
    if !Request(&User, "user", nil, false) {
        User = nil
        UserEve = nil
    } else {
        UserEve = User["eve"].(map[string]interface{})
        fmt.Printf("%v is a current EVE main\n", UserEve["charname"].(string))
    }
}

var netcl *http.Client

// NetInit sets up things. Call after ConfigLoad or Config.Proxy change.
func NetInit() {
    tr := &http.Transport{
        MaxIdleConns:    10,
        IdleConnTimeout: 30 * time.Second,
        //DisableCompression: true,
        Proxy: nil,
    }
    if Config.Proxy != "" {
        prxurl, err := url.Parse(Config.Proxy)
        if err == nil {
            tr.Proxy = http.ProxyURL(prxurl)
        }
    }
    //tr.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
    netcl = &http.Client{
        Transport: tr,
        Timeout:   60 * time.Second,
        CheckRedirect: func(r *http.Request, via []*http.Request) error {
            r.URL.Opaque = r.URL.Path
            return nil
        },
    }
}

// Request data from endpoint
func Request(out *map[string]interface{}, endpoint string, query map[string]interface{}, showerror bool) bool {
    // if netcl == nil {
    //     NetInit()
    // }
    if Config.Host == "" || Config.APIKey == "" {
        fmt.Printf("Not configured yet\n")
        return false
    }

    // build url
    rqurl, _ := url.Parse(Config.Host)
    rqurl.Path = "/api/" + endpoint
    rqurl.RawQuery = "apikey=" + Config.APIKey
    if query != nil {
        buf := new(bytes.Buffer)
        for key, value := range query {
            switch value.(type) {
            case string:
                fmt.Fprintf(buf, "&%s=%v", key, url.QueryEscape(value.(string)))
            default:
                fmt.Fprintf(buf, "&%s=%v", key, value)
            }
        }
        rqurl.RawQuery += buf.String()
    }
    fmt.Printf("Fetching URL: %v\n", rqurl.String())

    // make a request
    resp, err := netcl.Get(rqurl.String())
    if err != nil {
        if showerror {
            MsgBox("Cannot connect:\n" + err.Error())
        }
        return false
    }

    // parse the response
    defer resp.Body.Close()
    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        MsgBox("Error reading the response:\n" + err.Error())
    }

    //var res map[string]interface{}
    var txt interface{}
    err = json.Unmarshal(body, &txt)
    if err == nil {
        switch txt.(type) {
        case string:
            fmt.Printf("Server message: %s\n", txt)
            if txt.(string) != "OK" {
                if showerror {
                    MsgBox("Server Error:\n" + txt.(string))
                }
                return false
            }
            return true
        }
    }
    //dat, _ = json.Marshal("blah")
    err = json.Unmarshal(body, &out)
    if err != nil {
        if showerror {
            MsgBox("Error parsing the response:\n" + err.Error())
        }
        fmt.Printf("%s", body[:])
        return false
    }
    // if err != nil {
    //     txt, _ := ioutil.ReadAll(resp.Body)
    //     return txt
    // }
    //return res
    return true
}

// FileDL downloads file to filename destination
func FileDL(path, filename string) bool {
    // if netcl == nil {
    //     NetInit()
    // }
    if Config.Host == "" {
        fmt.Printf("Not configured yet\n")
        return false
    }

    // build url
    rqurl, _ := url.Parse(Config.Host)
    rqurl.Path = "/" + path
    fmt.Printf("Fetching URL: %v\n", rqurl.String())

    // make a request
    resp, err := netcl.Get(rqurl.String())
    if err != nil {
        return false
    }

    // parse the response
    defer resp.Body.Close()
    file, err := os.Create(filename)
    if err != nil {
        fmt.Printf("Error creating file: %v\n", err.Error())
        return false
    }
    size, err := io.Copy(file, resp.Body)
    defer file.Close()
    if err != nil {
        fmt.Printf("Error saving file: %v\n", err.Error())
        return false
    }
    fmt.Printf("Wrote %v bytes into %v\n", size, filename)

    return true
}
