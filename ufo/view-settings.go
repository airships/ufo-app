package ufo

import (
    "github.com/lxn/walk"
    ctl "github.com/lxn/walk/declarative"
    "os/exec"
    "runtime"
)

var settings *walk.Composite
var txtApikey *walk.LineEdit
var chkFx *walk.CheckBox

// Settings switches to and returns the settings view
func Settings() *walk.Composite {
    txtApikey.SetText(Config.APIKey)
    chkFx.SetEnabled(DwmIsCompositionEnabled())
    View(settings)
    return settings
}

// SettingsLoad loads and returns settings declarative view
func SettingsLoad() ctl.Composite {
    return ctl.Composite{
        AssignTo: &settings,
        Visible:  false,
        Font:     WndStyle.FontNormal,
        MinSize:  ctl.Size{0, 150},
        MaxSize:  ctl.Size{350, 150},
        //Layout:   ctl.Flow{Alignment: ctl.AlignHCenterVCenter},
        Layout: ctl.Grid{Columns: 3, Alignment: ctl.AlignHCenterVCenter},
        Children: []ctl.Widget{
            ctl.Label{Text: "API key:", MinSize: ctl.Size{70, 16}, MaxSize: ctl.Size{70, 24}},
            ctl.LineEdit{AssignTo: &txtApikey, MinSize: ctl.Size{200, 16}, MaxSize: ctl.Size{200, 24}},
            ctl.PushButton{
                Text:    "Test",
                MinSize: ctl.Size{40, 16},
                MaxSize: ctl.Size{40, 24},
                OnClicked: func() {
                    if txtApikey.Text() == "" {
                        txtApikey.SetFocus()
                        return
                    }
                    Waiting = true
                    var bck = Config.APIKey
                    Config.APIKey = txtApikey.Text()
                    UserQuery()
                    if User == nil {
                        Config.APIKey = bck
                        UserQuery()
                        MsgBox("API key looks invalid, can't get user info")
                    } else {
                        MsgBox("API key looking good!")
                    }
                    Waiting = false
                },
            },
            ctl.LinkLabel{
                ColumnSpan: 3,
                MinSize:    ctl.Size{300, 60},
                MaxSize:    ctl.Size{300, 60},
                Text:       `To get API key go to <a id="this" href="` + Config.Host + `/">LittleUFO website</a>, open menu at right, select API Console, then click API Keys and create one.`,
                OnLinkActivated: func(link *walk.LinkLabelLink) {
                    Open(link.URL())
                },
            },
            ctl.CheckBox{
                AssignTo:   &chkFx,
                ColumnSpan: 3,
                Text:       "Fancy window decorations (experimental)",
                OnCheckStateChanged: func() {
                    WinFX(chkFx.Checked())
                },
            },
        },
    }
}

// Open opens the specified URL in the default browser of the user.
// from https://github.com/icza/gowut
func Open(url string) error {
    var cmd string
    var args []string
    switch runtime.GOOS {
    case "windows":
        cmd = "cmd"
        args = []string{"/c", "start"}
    case "darwin":
        cmd = "open"
    default: // "linux", "freebsd", "openbsd", "netbsd"
        cmd = "xdg-open"
    }
    args = append(args, url)
    return exec.Command(cmd, args...).Start()
}
