package ufo

import (
    "github.com/lxn/walk"
    ctl "github.com/lxn/walk/declarative"
)

var fleet *walk.Composite

var fltTypes map[string]int = map[string]int{
    "Private":   0,
    "Roaming":   1,
    "OPS":       10,
    "Strat OPS": 11,
    "CTA":       100,
    "MAX CTA":   101,
}
var (
    txtName  *walk.LineEdit
    lstType  *walk.ComboBox
    btnFleet *walk.PushButton
)

// Fleet switches to and returns the fleet view
func Fleet() *walk.Composite {
    FleetUpd(true)
    View(fleet)
    return fleet
}

// FleetLoad loads and returns settings declarative view
func FleetLoad() ctl.Composite {
    return ctl.Composite{
        AssignTo: &fleet,
        Visible:  false,
        Font:     WndStyle.FontNormal,
        MinSize:  ctl.Size{0, 100},
        MaxSize:  ctl.Size{300, 100},
        Layout:   ctl.Grid{Columns: 2, Alignment: ctl.AlignHCenterVCenter},
        Children: []ctl.Widget{
            ctl.Label{Text: "Fleet name:"},
            ctl.LineEdit{
                AssignTo: &txtName,
                //Background: WndStyle.Background,
                // TODO: limit length
                // OnKeyDown: func(key walk.Key) {
                //     if key == walk.KeyReturn {
                //     }
                // },
            },
            ctl.Label{Text: "Fleet type:"},
            ctl.ComboBox{
                AssignTo: &lstType,
                //Background: WndStyle.Background,
                Editable: false,
                Model:    []string{"Private", "Roaming", "OPS", "Strat OPS", "CTA", "MAX CTA"},
            },
            ctl.PushButton{
                ColumnSpan: 2,
                AssignTo:   &btnFleet,
                //Background: WndStyle.Background,
                Text:    "Start",
                MinSize: ctl.Size{150, 24},
                MaxSize: ctl.Size{150, 24},
                OnClicked: func() {
                    fltAction()
                },
            },
            // TODO: display current fleet composition
        },
    }
}

// FleetUpd updates fleet controls. To include user input set "all" to true.
func FleetUpd(all bool) {
    if !wndLoaded {
        return
    }
    if all {
        txtName.SetText(UserEve["charname"].(string) + "'s fleet")
        lstType.SetCurrentIndex(0)
    }
    if UserEve["fc"].(bool) {
        fleet.SetEnabled(true)
        if UserEve["fleet"] == nil {
            txtName.SetEnabled(true)
            lstType.SetEnabled(true)
            btnFleet.SetText("Start")
        } else {
            txtName.SetEnabled(false)
            lstType.SetEnabled(false)
            btnFleet.SetText("Finish")
        }
    } else {
        fleet.SetEnabled(false)
        btnFleet.SetText("Not FC")
    }
}

func fltAction() {
    Waiting = true

    fname := txtName.Text()
    ftype := fltTypes[lstType.Text()]

    // TODO: check if name is ok with regexp

    if fname == "" {
        fname = UserEve["charname"].(string) + "'s fleet"
        txtName.SetText(fname)
    }

    var res map[string]interface{}
    if Request(&res, "fleet", map[string]interface{}{
        "action": "start",
        "name":   fname,
        "type":   ftype,
    }, true) {
        UserQuery()
        FleetUpd(false)
    }

    Waiting = false
}
