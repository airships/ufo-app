package ufo

import (
    "context"
    "fmt"
    "github.com/cxww107/asyncwork/worker"
    "github.com/lxn/walk"
    ctl "github.com/lxn/walk/declarative"
    "github.com/lxn/win"
    "os"
    "time"
)

var title string = "LittleUFO App v.0.1"

type wndMain struct {
    *walk.MainWindow
}

var (
    wndLoaded bool
    wnd       *wndMain
    hwnd      win.HWND
    wndSize   walk.Size
    container []ctl.Widget
    waiter    *walk.ProgressBar
    ufoview   *walk.ImageView
    viewcur   *walk.Composite
    tabs      *walk.Composite
    firsttime bool = true
)

// Waiting - visually indicate waiting status
var Waiting bool = true

// App starts the app.
func App() {
    // load config
    ConfigLoad()
    // init networking
    NetInit()
    // start background tasks
    bgTasks()
    // create main window
    createWindow()
    // enter main loop until exit
    wnd.Run()
    // save config on exit
    ConfigSave()
}

func createWindow() {
    ufoico, _ := walk.NewIconExtractedFromFile("ufo-app.exe", 0, 0) // TODO: extract a bigger icon
    var ufoimg walk.Image
    if _, err := os.Stat(CfgPath + "/ufo-app.png"); err == nil {
        ufoimg, _ = walk.NewImageFromFileForDPI(CfgPath+"/ufo-app.png", 96)
    } else {
        ufoimg, _ = walk.ImageFrom(ufoico)
    }

    // initial window size
    Resize(300, 160)

    container = []ctl.Widget{
        ctl.Composite{
            AssignTo:   &tabs,
            Visible:    false,
            MinSize:    ctl.Size{250, WndStyle.TabHeight},
            MaxSize:    ctl.Size{1000, WndStyle.TabHeight},
            Background: WndStyle.Background,
            Layout:     ctl.Grid{Rows: 1, Alignment: ctl.AlignHCenterVCenter},
            Children: []ctl.Widget{
                ctl.HSpacer{MinSize: ctl.Size{0, 5}, MaxSize: ctl.Size{50, 5}},
                ctl.LinkLabel{
                    MinSize:         ctl.Size{100, 0},
                    MaxSize:         ctl.Size{100, 0},
                    Font:            WndStyle.FontTab,
                    Text:            `<a id="this" href="#">Fleet</a>`,
                    OnLinkActivated: func(link *walk.LinkLabelLink) { Fleet() },
                },
                ctl.HSpacer{MinSize: ctl.Size{0, 5}, MaxSize: ctl.Size{50, 5}},
                ctl.LinkLabel{
                    MinSize:         ctl.Size{100, 0},
                    MaxSize:         ctl.Size{100, 0},
                    Font:            WndStyle.FontTab,
                    Text:            `<a id="this" href="#">Settings</a>`,
                    OnLinkActivated: func(link *walk.LinkLabelLink) { Settings() },
                },
                ctl.HSpacer{MinSize: ctl.Size{0, 5}, MaxSize: ctl.Size{50, 5}},
            },
        },
        ctl.ImageView{
            AssignTo: &ufoview,
            Image:    ufoimg,
            Mode:     ctl.ImageViewModeCenter,
            MinSize:  ctl.Size{wndSize.Width, wndSize.Height},
            MaxSize:  ctl.Size{wndSize.Width, wndSize.Height},
        },
        FleetLoad(),
        SettingsLoad(),
        ctl.ProgressBar{
            AssignTo:    &waiter,
            MinSize:     ctl.Size{0, WndStyle.WaitHeight},
            MaxSize:     ctl.Size{1000, WndStyle.WaitHeight},
            MarqueeMode: true,
        },
    }

    // main window
    wnd = &wndMain{}
    ctl.MainWindow{
        AssignTo:   &wnd.MainWindow,
        Children:   container,
        Visible:    false,
        Icon:       ufoico,
        Title:      title,
        Layout:     ctl.VBox{MarginsZero: true},
        Background: WndStyle.WindowBg,
    }.Create()
    hwnd = wnd.Handle()

    // set window styles
    win.SetWindowLong(hwnd, win.GWL_STYLE, win.WS_BORDER|win.WS_CAPTION|win.WS_SYSMENU|win.WS_MINIMIZEBOX)
    //win.SetWindowLong(hwnd, win.GWL_STYLE, win.WS_VISIBLE)

    // center window
    var wndWidth int32 = int32(wndSize.Width)
    var wndHeight int32 = int32(wndSize.Height)
    xScreen := win.GetSystemMetrics(win.SM_CXSCREEN)
    yScreen := win.GetSystemMetrics(win.SM_CYSCREEN)
    win.SetWindowPos(hwnd, 0,
        (xScreen-wndWidth)/2, (yScreen-wndHeight)/2,
        wndWidth, wndHeight,
        win.SWP_FRAMECHANGED)
    // show it
    win.ShowWindow(hwnd, win.SW_SHOW)

    // apply fx (and bg for some reason lol)
    WinFX(false)

    // set loaded flag
    wndLoaded = true

    // reset size
    Resize(0, 0)

    Waiting = false

    TitleUpd()
}

// WinFX enables or disables window decorations
func WinFX(enable bool) {
    var fx bool
    var winver uint32
    if enable {
        winver = win.GetVersion() & 0xFF
        //major, minor := winver&0xFF, winver&0xFF00>>8
        if winver < 6 {
            MsgBox("Supported Windows version is Vista and above")
        } else {
            fx = DwmIsCompositionEnabled()
            if !fx {
                MsgBox("Enable theming in Windows")
            }
        }
    }
    wbg := WndStyle.WindowBg

    if fx {
        // apply fx
        if winver < 10 {
            // aero
            win.SetWindowLong(hwnd, win.GWL_EXSTYLE, WS_EX_NOREDIRECTIONBITMAP|win.WS_EX_LAYERED)
            Aero(uintptr(hwnd), wndSize.Width, wndSize.Height)
        } else {
            // fluent
            win.SetWindowLong(hwnd, win.GWL_EXSTYLE, WS_EX_NOREDIRECTIONBITMAP)
            //Fluent(uintptr(hwnd), AsEnableAcrylicBlurBehind, true, 0X55101513)
            Fluent(uintptr(hwnd), AsEnableBlurBehind, true, 0)
            // NOTE: to elimitane double borders, you should disable window borders at all, such as WS_VISIBLE only
        }
        // other things
        //SetLayeredWindowAttributes(uintptr(hwnd), 0, 170, LWA_ALPHA)
        //SetLayeredWindowAttributes(uintptr(hwnd), 0X00FFFFFF, 0, LWA_COLORKEY)
        //SetLayeredWindowAttributes(uintptr(hwnd), uint32(walk.RGB(255, 0, 255)), 0, LWA_COLORKEY)

        wbg = WndStyle.WindowBgAlpha
    } else {
        win.SetWindowLong(hwnd, win.GWL_EXSTYLE, win.WS_EX_APPWINDOW)
        Fluent(uintptr(hwnd), AsDisabled, false, 0)
    }

    brh, _ := wbg.Create()
    wnd.SetBackground(brh)
}

func bgTasks() {
    TaskRun(
        []worker.TaskFunction{
            // waiting status watcher
            func() interface{} {
                for {
                    if wndLoaded {
                        // TODO: switch cursor
                        if waiter.MarqueeMode() != Waiting {
                            if viewcur != nil {
                                tabs.SetEnabled(!Waiting)
                                viewcur.SetEnabled(!Waiting)
                            }
                            waiter.SetMarqueeMode(Waiting)
                        }
                    }
                    time.Sleep(time.Millisecond * 100)
                }
            },
            // user updates polling
            func() interface{} {
                for {
                    UserQuery()
                    TitleUpd()
                    FleetUpd(false)
                    if firsttime {
                        time.Sleep(time.Second * 1)
                        // if config is fine and checked remotely, and if user is FC, display fleets; else settings or message.
                        // if config is not fine or key is invalid, display settings
                        if UserEve != nil && UserEve["fc"].(bool) {
                            Fleet()
                        } else {
                            Settings()
                        }
                        firsttime = false
                    }
                    time.Sleep(time.Second * 5)
                }
            },
            // startup image saver
            func() interface{} {
                FileDL("media/ufo-app.png", CfgPath+"/ufo-app.png")
                ufoimg, err := walk.NewImageFromFileForDPI(CfgPath+"/ufo-app.png", 96)
                if err != nil {
                    ufoview.SetImage(ufoimg)
                }
                return nil
            },
        },
        false, // don't wait
    )
}

var curttl string

// TitleUpd updates the title
func TitleUpd() {
    if !wndLoaded {
        return
    }
    var newttl string = curttl
    if newttl != "" {
        newttl += " - "
    }
    if UserEve != nil {
        newttl += UserEve["charname"].(string) + " - "
    }
    newttl += title
    wnd.SetTitle(newttl)
}

// Title sets main window title
func Title(ttl string) {
    curttl = ttl
    TitleUpd()
}

// Resize the main window
func Resize(width, height int) {
    if width > 0 {
        wndSize.Width = width
    }
    if height > 0 {
        wndSize.Height = height
    }
    if !wndLoaded {
        return
    }
    //win.SetWindowPos(hwnd, 0, 0, 0, int32(wndSize.Width), int32(wndSize.Height), win.SWP_NOMOVE)
    wnd.SetClientSize(wndSize)
    //fmt.Printf("%v\n", wndSize)
}

// View switches to another view. View's MaxSize controls window size.
func View(newview *walk.Composite) {
    if !wndLoaded {
        return
    }
    var sz walk.Size
    if newview != nil {
        if newview.Visible() {
            return
        }
        if viewcur == nil {
            ufoview.SetVisible(false)
        } else {
            viewcur.SetVisible(false)
        }
        tabs.SetVisible(true)
        viewcur = newview
        viewcur.SetVisible(true)
        sz = viewcur.MaxSize()
        sz.Height += WndStyle.TabHeight
    } else {
        if ufoview.Visible() {
            return
        }
        tabs.SetVisible(false)
        ufoview.SetVisible(true)
        sz = ufoview.MaxSize()
    }
    sz.Height += WndStyle.WaitHeight
    Resize(sz.Width, sz.Height)
}

// MsgBox displays message box
func MsgBox(msg string) {
    if wndLoaded {
        walk.MsgBox(
            wnd.MainWindow,
            title,
            msg,
            walk.MsgBoxOK|walk.MsgBoxIconWarning,
        )
    } else {
        walk.MsgBox(
            nil,
            title,
            msg,
            walk.MsgBoxOK|walk.MsgBoxIconWarning,
        )
    }
}

// TaskRun starts a new task
func TaskRun(tasks []worker.TaskFunction, wait bool) interface{} {
    // task1 := func() interface{} {
    //     fmt.Println("very slow function")
    //     time.Sleep(time.Second * 4)
    //     return "I'm ready"
    // }
    // task2 := func() interface{} {
    //     fn[0]()
    //     return "done"
    // }
    // tasks := []worker.TaskFunction{task1, task2}
    ctx, cancel := context.WithCancel(context.Background())
    defer cancel()
    var res []interface{} = []interface{}{}
    if !wait {
        // run
        worker.PerformTasks(ctx, tasks)
    } else {
        // run and wait
        chnResult := worker.PerformTasks(ctx, tasks)
        for result := range chnResult {
            // rl := len(res)
            // if result != nil && rl > 0 {
            //     res = res[0 : rl+1]
            // }
            switch result.(type) {
            case error:
                fmt.Printf("Received error: %v\n", result.(error).Error())
                // res[rl] = result.(error).Error()
                // fmt.Printf("Received error: %v\n", res[rl])
                cancel()
                return nil
            default:
                // if result != nil {
                //     res[rl] = result
                // }
            }
        }
    }
    return res
}
