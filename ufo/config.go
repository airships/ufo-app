package ufo

import (
    "encoding/json"
    "fmt"
    //"github.com/lxn/walk"
    "io/ioutil"
    "os"
)

// ConfigType struct for a Config variable
type ConfigType struct {
    UserAgent string `json:"-"`
    Proxy     string `json:"proxy,omitempty"`
    Host      string `json:"host,omitempty"`
    APIKey    string `json:"apikey,omitempty"`
}

// Config variables
var Config *ConfigType
var cfgTpl ConfigType = ConfigType{
    UserAgent: title,
    Proxy:     "",
    Host:      "https://ufo.scalie.net",
    APIKey:    "",
}

// CfgPath path to config folder
var CfgPath string = "LittleUFO App"
var cfgFile string = "ufo-app.json"

//var cfg *walk.IniFileSettings

// ConfigLoad loads config file. Who could have guessed?
func ConfigLoad() {
    // old cfg location
    pth := os.Getenv("APPDATA")
    oldloc := pth + "/" + cfgFile
    // current cfg location
    pth, err := os.UserConfigDir()
    CfgPath = pth + "/" + CfgPath
    os.MkdirAll(CfgPath, os.ModeDir)
    cfgFile = CfgPath + "/" + cfgFile
    if _, err := os.Stat(oldloc); err == nil {
        os.Rename(oldloc, cfgFile)
    }

    data, err := ioutil.ReadFile(cfgFile)
    if err != nil {
        MsgBox("load error: " + err.Error())
    } else {
        var obj ConfigType = cfgTpl
        err = json.Unmarshal(data, &obj)
        if err != nil {
            MsgBox("parse error: " + err.Error())
        } else {
            Config = &obj
        }
    }

    // app := walk.App()
    // app.SetOrganizationName("LittleUFO")
    // app.SetProductName("LittleUFO App")
    // cfg = walk.NewIniFileSettings("settings.ini")
    // app.SetSettings(cfg)
}

// ConfigSave saves config file. Wow.
func ConfigSave() {
    data, err := json.MarshalIndent(Config, "", "  ")
    if err != nil {
        fmt.Println("error:", err)
    }

    err = ioutil.WriteFile(cfgFile, data, 0644)
    if err != nil {
        fmt.Print(err)
    }

    // if err := cfg.Save(); err != nil {
    //     fmt.Println("error saving:", err)
    // }
}
