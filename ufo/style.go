package ufo

import (
	"github.com/lxn/walk"
	ctl "github.com/lxn/walk/declarative"
)

// WndStyle holds variables for window and controls styles
var WndStyle StyleType = StyleType{
	TabHeight:  35,
	WaitHeight: 5,
	WindowBg: ctl.GradientBrush{
		Vertexes: []walk.GradientVertex{
			{X: 0, Y: 0, Color: walk.RGB(100, 110, 105)},
			{X: 1, Y: 0, Color: walk.RGB(200, 200, 200)},
			{X: 0.5, Y: 0.5, Color: walk.RGB(200, 200, 200)},
			{X: 1, Y: 1, Color: walk.RGB(100, 110, 105)},
			{X: 0, Y: 1, Color: walk.RGB(200, 200, 200)},
		},
		Triangles: []walk.GradientTriangle{
			{0, 1, 2},
			{1, 3, 2},
			{3, 4, 2},
			{4, 0, 2},
		},
	},
	WindowBgAlpha: ctl.GradientBrush{
		Vertexes: []walk.GradientVertex{
			{X: 0, Y: 0, Color: walk.RGB(0, 0, 0)},
			{X: 1, Y: 0, Color: walk.RGB(150, 160, 155)},
			{X: 0.5, Y: 0.5, Color: walk.RGB(150, 160, 155)},
			{X: 1, Y: 1, Color: walk.RGB(0, 0, 0)},
			{X: 0, Y: 1, Color: walk.RGB(150, 160, 155)},
		},
		Triangles: []walk.GradientTriangle{
			{0, 1, 2},
			{1, 3, 2},
			{3, 4, 2},
			{4, 0, 2},
		},
	},
	Background: ctl.SolidColorBrush{Color: walk.RGB(150, 160, 155)},
	//Background: ctl.SolidColorBrush{Color: 0X00FFFFFF},
	Color: walk.RGB(200, 210, 205),
	FontNormal: ctl.Font{Family: "Arial", PointSize: 12,
		Bold: false, Italic: false, Underline: false, StrikeOut: false},
	FontTitle: ctl.Font{Family: "Arial", PointSize: 16,
		Bold: false, Italic: true, Underline: false, StrikeOut: false},
	FontTab: ctl.Font{Family: "Arial", PointSize: 14,
		Bold: false, Italic: false, Underline: false, StrikeOut: false},
	FontTabSel: ctl.Font{Family: "Arial", PointSize: 14,
		Bold: true, Italic: false, Underline: false, StrikeOut: false},
}

// StyleType obv
type StyleType struct {
	TabHeight     int
	WaitHeight    int
	WindowBg      ctl.Brush
	WindowBgAlpha ctl.Brush
	Background    ctl.Brush
	Color         walk.Color
	FontNormal    ctl.Font
	FontTitle     ctl.Font
	FontTab       ctl.Font
	FontTabSel    ctl.Font
}
