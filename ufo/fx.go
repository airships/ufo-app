package ufo

import (
    "syscall"
    "unsafe"
)

// Aero (vista, win7)
func Aero(hwnd uintptr, width int, height int) {
    // if !DwmIsCompositionEnabled() {
    //     return
    // }
    // one approach
    DwmEnableBlurBehindWindow(hwnd, &dwmBlurBehind{
        DwFlags:                1,
        fEnable:                1,
        hRgnBlur:               CreateEllipticRgn(0, 0, width, height),
        fTransitionOnMaximized: 0,
    })
    // other approach
    //DwmExtendFrameIntoClientArea(hwnd, &margins{-1, -1, -1, -1})
    //DwmExtendFrameIntoClientArea(hwnd, &margins{0, 0, int32(width), int32(height)})
}

// Fluent (win10 1803+) Set set WS_EX_NOREDIRECTIONBITMAP for faster rendering
func Fluent(hwnd uintptr, mode int, frame bool, abgrTint uint32) {
    // if !DwmIsCompositionEnabled() {
    //     return
    // }
    accent := accentPolicy{
        accentState:   mode,
        accentFlags:   0,
        gradientColor: abgrTint,
        animationID:   0,
    }
    if frame { // with window frame
        accent.accentFlags = 0x20 | 0x40 | 0x80 | 0x100
    }
    wca := windowCompositionAttribData{
        dwAttrib: wcaAccentPolicy,
        pvData:   &accent,
        cbData:   unsafe.Sizeof(accent),
    }
    SetWindowCompositionAttribute(hwnd, &wca)
}

const (
    // WS_EX_NOREDIRECTIONBITMAP faster drawing windows pipeline
    WS_EX_NOREDIRECTIONBITMAP int32 = 0X00200000
    // LWA_COLORKEY transparency by colour for layered window
    LWA_COLORKEY = 0X00000001
    // LWA_ALPHA transparency by alpha value for layered window
    LWA_ALPHA = 0X00000002
)

const (
    // Accent State
    AsDisabled = iota
    AsEnableGradient
    AsEnableTransparentGradient
    AsEnableBlurBehind
    AsEnableAcrylicBlurBehind // lags on window position/size change
    AsInvalidState
)

var (
    // Aero
    dwmapi                           *syscall.LazyDLL  = syscall.NewLazyDLL("dwmapi.dll")
    procDwmEnableBlurBehindWindow    *syscall.LazyProc = dwmapi.NewProc("DwmEnableBlurBehindWindow")
    procDwmIsCompositionEnabled      *syscall.LazyProc = dwmapi.NewProc("DwmIsCompositionEnabled")
    procDwmExtendFrameIntoClientArea *syscall.LazyProc = dwmapi.NewProc("DwmExtendFrameIntoClientArea")
    gdi32                            *syscall.LazyDLL  = syscall.NewLazyDLL("gdi32.dll")
    procCreateEllipticRgn            *syscall.LazyProc = gdi32.NewProc("CreateEllipticRgn")
    // Fluent
    user32                            *syscall.LazyDLL  = syscall.NewLazyDLL("user32.dll")
    procSetWindowCompositionAttribute *syscall.LazyProc = user32.NewProc("SetWindowCompositionAttribute")
    procSetLayeredWindowAttributes    *syscall.LazyProc = user32.NewProc("SetLayeredWindowAttributes")
)

type dwmBlurBehind struct {
    DwFlags                uint32
    fEnable                int
    hRgnBlur               uintptr
    fTransitionOnMaximized int
}

const wcaAccentPolicy uint32 = 19

type windowCompositionAttribData struct {
    dwAttrib uint32
    pvData   *accentPolicy
    cbData   uintptr
}

type accentPolicy struct {
    accentState   int
    accentFlags   uint32
    gradientColor uint32
    animationID   uint32
}

type margins struct {
    cxLeftWidth, cxRightWidth, cyTopHeight, cyBottomHeight int32
}

// CreateEllipticRgn for aero region
func CreateEllipticRgn(x1, y1, x2, y2 int) uintptr {
    ret, _, _ := procCreateEllipticRgn.Call(uintptr(x1), uintptr(y1), uintptr(x2), uintptr(y2))
    if ret == 0 {
        panic("Create elliptic region failed")
    }
    return uintptr(ret)
}

// DwmEnableBlurBehindWindow apply aero
func DwmEnableBlurBehindWindow(hWnd uintptr, pBlurBehind *dwmBlurBehind) int32 {
    ret, _, _ := procDwmEnableBlurBehindWindow.Call(hWnd, uintptr(unsafe.Pointer(pBlurBehind)))
    return int32(ret)
}

// DwmExtendFrameIntoClientArea apply aero
func DwmExtendFrameIntoClientArea(hWnd uintptr, pMarInset *margins) int32 {
    ret, _, _ := procDwmExtendFrameIntoClientArea.Call(hWnd, uintptr(unsafe.Pointer(pMarInset)))
    return int32(ret)
}

// DwmIsCompositionEnabled check if aero is enabled
func DwmIsCompositionEnabled() bool {
    var aero bool
    procDwmIsCompositionEnabled.Call(uintptr(unsafe.Pointer(&aero)))
    return aero
}

// SetWindowCompositionAttribute fluent acryllic glass enabler
func SetWindowCompositionAttribute(hWnd uintptr, pWndCompAttrData *windowCompositionAttribData) bool {
    ret, _, _ := procSetWindowCompositionAttribute.Call(hWnd, uintptr(unsafe.Pointer(pWndCompAttrData)))
    if ret == 0 {
        return false
    }
    return true
}

// SetLayeredWindowAttributes sets the opacity and transparency color key of a layered window
func SetLayeredWindowAttributes(hWnd uintptr, crKey uint32, bAlpha byte, dwFlags uint32) {
    procSetLayeredWindowAttributes.Call(hWnd, uintptr(crKey), uintptr(bAlpha), uintptr(dwFlags))
}
