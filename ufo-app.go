package main

//go:generate goversioninfo -o rsrc.syso -icon=build/ufo.ico -manifest=build/ufo.manifest -ver-major=0 -ver-minor=1 -ver-patch=0 -ver-build=0 -product-name=LittleUFO-App -description=LittleUFO-App -comment=https://gitlab.com/airships/ufo-app -copyright=Shyaltii(MIT-license)

import . "./ufo"

func main() {
    App()
}
