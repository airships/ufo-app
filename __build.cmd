@echo off

IF [%1] EQU [run] (
  set OUT=build\ufo-app-test.exe
  set "FLAGS="
) ELSE (
  set OUT=build\ufo-app.exe
  set FLAGS=-tags walk_use_cgo
  set FLAGS=-ldflags=^"-H windowsgui^"
)

IF EXIST %OUT% del /Q %OUT%

set GOOS=windows
set GOARCH=386

echo Generating resources...

:go get github.com\akavel\rsrc
:rsrc -manifest build/ufo.manifest -ico build/ufo.ico -arch="%GOARCH%" -o rsrc.syso

:go get github.com/josephspurrier/goversioninfo/cmd/goversioninfo
echo {}>versioninfo.json
go generate
del versioninfo.json

IF NOT EXIST rsrc.syso (
  echo Resource generation failed
  pause
  goto end
)

echo Building %OUT%...

go build -o %OUT% %FLAGS%

del rsrc.syso

IF NOT EXIST %OUT% (
  echo Build failed
  pause
  goto end
)

cd build

IF [%1] EQU [run] (
  echo Running...
  call cmd /c ..\%OUT%
  del /Q ..\%OUT%
  IF NOT ERRORLEVEL 0 pause
) ELSE (
  rem echo Continue to run built app?
  rem pause
  rem start ..\%OUT%
  echo Continue to zip it?
  pause
  powershell -ExecutionPolicy Bypass .\zip.ps1
)

:end
